cmake_minimum_required(VERSION 2.8)
project(sldebug)

if(COMMAND CMAKE_POLICY)
	CMAKE_POLICY(SET CMP0005 NEW)
	CMAKE_POLICY(SET CMP0003 NEW)
	CMAKE_POLICY(SET CMP0020 NEW)
endif(COMMAND CMAKE_POLICY)

set(CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake)

include(CopyDLL)
include(BuildOptions)

set(VERSION_MAJOR "0")
set(VERSION_MINOR "1")
set(VERSION_PATCH "0")
set(VERSION_SUFFIX "b")

if(NOT CMAKE_RUNTIME_OUTPUT_DIRECTORY)
  set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/bin${PROGRAM_SUFFIX})
endif(NOT CMAKE_RUNTIME_OUTPUT_DIRECTORY)

set(VERSION "${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH}")
if(VERSION_SUFFIX)
	set(VERSION "${VERSION}${VERSION_SUFFIX}")
endif(VERSION_SUFFIX)

if(APPLE)
  set(CMAKE_MODULE_PATH /usr/local/opt/qt/lib/cmake/Qt5)
  set(Qt5_DIR /usr/local/opt/qt/lib/cmake/Qt5)
endif(APPLE)

if(WIN32)
  set(QT_SYSTEM_LIBS Qt5::WinMain)
endif(WIN32)

set(BUILD_BINARIES OFF)
add_subdirectory(signalogic)

set(QT_MIN_VERSION "5.0.0")
find_package(Qt5 COMPONENTS Widgets Core REQUIRED)
include_directories(${PROJECT_SOURCE_DIR}/signalogic/src)
include_directories(${Qt5Widgets_INCLUDE_DIRS} ${Qt5Core_INCLUDE_DIRS} ${CMAKE_BINARY_DIR})

configure_file(${CMAKE_SOURCE_DIR}/sldebug.rc.in ${CMAKE_BINARY_DIR}/sldebug.rc)

file(GLOB sldebug_SOURCES src/*.cpp)
file(GLOB sldebug_HEADERS src/*.h)
file(GLOB sldebug_UI      ui/*.ui)

qt5_wrap_cpp(sldebug_MOC_out ${sldebug_HEADERS})
qt5_wrap_ui(sldebug_UIC_out ${sldebug_UI})
qt5_add_resources(sldebug_RCC_out data/sldebug.qrc)

add_executable(sldebug WIN32 ${sldebug_SOURCES} ${sldebug_UIC_out} ${sldebug_MOC_out} ${sldebug_RCC_out})
target_link_libraries(sldebug ${Qt5Widgets_LIBRARIES} ${Qt5Core_LIBRARIES} client++ ${QT_SYSTEM_LIBS})

if(WIN32)
  copy_dll(package-dlls ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})
endif(WIN32)
