/***************************************************
 * qclient.h
 * Created on Thu, 15 Aug 2019 18:44:52 +0000 by vladimir
 *
 * $Author$
 * $Rev$
 * $Date$
 ***************************************************/
#pragma once

#include <QSocketNotifier>
#include <QObject>
#include <cpp/Client.hpp>

class QClient : public QObject, public Client
{
  Q_OBJECT
  QSocketNotifier *m_socketNotifier;
  QSocketNotifier *m_eventSocketNotifier;

  virtual void onConnected() override;
  virtual void onDisconnected() override;
  virtual void onWriteSignalValue(Signal *s, const SignalValue &v) override;
  virtual void onWriteSignalError(Signal *s) override;
  virtual void onUpdateSignalValue(Signal *s, const SignalValue &v) override;
  virtual void onUpdateSignalState(Signal *s, const SignalState &v) override;
  virtual void onAddSignal(Signal *s) override;
  virtual void onDeleteSignal(Signal *s) override;

public:
  QClient(QObject *parent = NULL);
  ~QClient();

  void connect(const QString &addr, int port) {
    delete m_socketNotifier;
    delete m_eventSocketNotifier;
    m_socketNotifier = m_eventSocketNotifier = NULL;
    Client::connect(addr.toStdString().c_str(), port);
  }

  void disconnect() {
    Client::disconnect();
  }

protected Q_SLOTS:
  void run(int socket);

Q_SIGNALS:
  void connected(); //!< Connected signal - triggered when server has been connected
  void disconnected(); //!< Disconnected signal - triggered when server has been disconnected
  void writeSignalValue(Signal *s, const SignalValue &v); //!< Write signal value signal - triggered when a write occurs
  void writeSignalError(Signal *s); //!< Write signal error signal - triggered when a write failed
  void updateSignalValue(Signal *s, const SignalValue &v); //!< Update signal value signal - triggered when a signal update occurs
  void updateSignalState(Signal *s, const SignalState &v); //!< Update signal state signal - triggered when a signal state updates
  void addSignal(Signal *s); //!< Add signal signal - triggered when a signal is added to the system
  void deleteSignal(Signal *s); //!< Delete signal signal - triggered when a signal is deleted from the system
};

/*
Signal interface description: 

class SignalState {
public:
  SignalState(struct signal_state_s *state);
  SignalState(bool rf, bool wf);
  SignalState(const SignalState &state);
  SignalState();
  ~SignalState();

  bool &readFailure();
  bool &writeFailure();

  const bool &readFailure() const;
  const bool &writeFailure() const;

  SignalState &operator=(const SignalState &other);

private:
  struct signal_state_s mState;
  struct signal_state_s *mPState;
};

class SignalValue {
public:
  SignalValue();
  SignalValue(const SignalValue &);
  SignalValue(long v);
  SignalValue(const std::string &v);
  SignalValue(const std::vector<char> &v);
  SignalValue(struct signal_value_s *value);
  ~SignalValue();

  const SignalValue &operator =(const SignalValue &v); // Set from value
  const std::string &operator =(const std::string &s); // Set from string
  const std::vector<char> &operator =(const std::vector<char> &v); // Set from vector of chars*
  long operator =(long l); // Set from long

  bool isString() const;
  bool isBlob() const;
  bool isLong() const;

  operator std::string() const; // Convert to string
  operator std::vector<char>() const; // Convert to vector of chars (BLOB)
  operator long() const; // Convert to long
  operator char*() const; // Convert to char*
  operator signal_value_s&();
  operator signal_value_s*();

  int type() const; // Get type: one of ST_INT, ST_TEXT, ST_BLOB
  int size() const; // Get blob or string size

  std::string printable() const; // Convert to printable value (TYPE:value)
  void fromPrintable(const std::string &value); // Parse printable value to signal value

private:
  struct signal_value_s mValue;
  struct signal_value_s *mPValue;
};

class Signal {
public:
  Signal(struct execution_context_s *ctx, struct signal_s *signal);

  void read();
  void write(const SignalValue &val);
  void update(const SignalValue &val);
  void update(const SignalState &val);
  void subscribe(int type);
  void unsubscribe(int type);

  void *getUserData();
  void setUserData(void *);
  template<typename T>
  T* userData() {
    return (T*)getUserData();
  }
  template<typename T>
  void setUserData(T *data) {
    setUserData((void*)data);
  }

  int id() const;
  const char *name() const;
  const char *type() const;
  const char *path() const;
  const SignalValue &value() const;
  const SignalState &state() const;

  Signal *next() const;

private:
  struct execution_context_s *mContext;
  struct signal_s *mSignal;
  SignalValue mValue;
  SignalState mState;
  void *mUserData;
};
*/

/*
Client interface description:
class Client
  : public ClientHandler
{
public:
  Client();
  virtual ~Client();

  void init();
  void destroy();

  std::list<Signal *> getSignals();
  std::list<Signal *> findSignalsByMask(const std::string &mask);
  std::list<Signal *> findSignalsByType(const std::string &type);
  Signal *findSignal(const std::string &name);
  Signal *findSignal(int id);

  void connect(const char *address, int port);
  void disconnect();
  int process(int timeout_usec);
  bool isRunning();
  void setRunning(bool);
  int setResponseTimeout(int to);
  int getSocket();
  int getEventSocket();

  // The functions below work with signal specification. It can be
  // ID, Mask, Name or Type. Depending on the type, the specification itself
  // will be int, or const char*
  //
  // type: one of SS_ID, SS_MASK, SS_NAME, SS_TYPE
  // SS_ID: next argument is the integer ID of the signal (signal->id())
  // SS_MASK: next argument is char* - the mask of the signals
  // SS_NAME: next argument is char* - the name of the signal (strict match)
  // SS_TYPE: next argument is char* - the type of the signals

  // Read: read signals by spec
  template<typename ... Ts>
  int read(int type, Ts... xs) {
    return read_command(mContext, type, xs...);
  }

  // Write: write signals by spec. Spec is followed by value type and value. Value types can be following:
  // ST_INT, ST_TEXT, ST_BLOB
  // ST_INT: followed by int32_t argument
  // ST_TEXT: followed by char* containing null-terminated string
  // ST_BLOB: followed by int size and char* containing exactly `size` number of bytes
  template<typename ... Ts>
  int write(int type, Ts... xs) {
    return write_command(mContext, type, xs...);
  }

  // Update: update signals by spec. Spec is followed by the type of the update and then the update itself.
  // Update type can be SST_VALUE, SST_RDERR, SST_WRERR
  // SST_VALUE is followed by value argument, which is value type and value.
  // Value types can be following:
  // - ST_INT, ST_TEXT, ST_BLOB
  // - ST_INT: followed by int32_t argument
  // - ST_TEXT: followed by char* containing null-terminated string
  // - ST_BLOB: followed by int size and char* containing exactly `size` number of bytes
  // SST_RDERR and SST_WRERR are followed by int argument: 0 - no error occured, 1 - error occured.
  template<typename ... Ts>
  int update(int type, Ts... xs) {
    return update_command_do(mContext, type, xs..., -1);
  }

  // Subscribe for signal events: sub_type is SUB_WRITE or SUB_UPDATE
  template<typename ... Ts>
  int subscribe(int sub_type, int type, Ts... xs) {
    return subscribe_command(mContext, sub_type, type, xs...);
  }

  // Unsubscribe from signal events: sub_type is SUB_WRITE or SUB_UPDATE
  template<typename ... Ts>
  int unsubscribe(int sub_type, int type, Ts... xs) {
    return unsubscribe_command(mContext, sub_type, type, xs...);
  }

  // Post commands - do same as plain commands, but executed when client `process` function is called.
  // Should be followed by postProcess to trigger processing (in case if several queued commands are posted)

  template<typename ... Ts>
  void postRead(int type, Ts... xs) {
    post_read_command(mContext, type, xs...);
  }

  template<typename ... Ts>
  void postWrite(int type, Ts... xs) {
    post_write_command(mContext, type, xs...);
  }

  template<typename ... Ts>
  void postUpdate(int type, Ts... xs) {
    post_update_command_do(mContext, type, xs..., -1);
  }

  template<typename ... Ts>
  void postSubscribe(int sub_type, int type, Ts... xs) {
    post_subscribe_command(mContext, sub_type, type, xs...);
  }

  template<typename ... Ts>
  void postUnsubscribe(int sub_type, int type, Ts... xs) {
    post_unsubscribe_command(mContext, sub_type, type, xs...);
  }

  void postProcess();

private:
  execution_context_s *mContext;

  static void event_write_signal_value(struct execution_context_s *ctx, struct signal_s *signal, struct signal_value_s *value);
  static void event_write_signal_error(struct execution_context_s *ctx, struct signal_s *signal, struct signal_value_s *value);
  static void event_update_signal_value(struct execution_context_s *ctx, struct signal_s *signal, struct signal_value_s *value);
  static void event_update_signal_state(struct execution_context_s *ctx, struct signal_s *signal, struct signal_state_s *value);
  static void event_add_signal(struct execution_context_s *ctx, struct signal_s *signal);
  static void event_delete_signal(struct execution_context_s *ctx, struct signal_s *signal);
};
*/
