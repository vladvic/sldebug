/***************************************************
 * qclient.cpp
 * Created on Thu, 15 Aug 2019 18:46:49 +0000 by vladimir
 *
 * $Author$
 * $Rev$
 * $Date$
 ***************************************************/

#include <QDebug>
#include "QClient.h"

QClient::QClient(QObject *parent)
  : m_socketNotifier(NULL)
  , m_eventSocketNotifier(NULL)
{
  Client::init();
}

QClient::~QClient() {
  disconnect();
  Client::destroy();
}

void QClient::onConnected() {
  delete m_socketNotifier;
  delete m_eventSocketNotifier;

  m_socketNotifier = m_eventSocketNotifier = NULL;

  m_socketNotifier = new QSocketNotifier(getSocket(), QSocketNotifier::Read, this);
  m_eventSocketNotifier = new QSocketNotifier(getEventSocket(), QSocketNotifier::Read, this);

#ifdef __WIN32__
  QObject::connect(m_socketNotifier, &QSocketNotifier::activated, this, &QClient::run);
  QObject::connect(m_eventSocketNotifier, &QSocketNotifier::activated, this, &QClient::run);
#else
  QObject::connect(m_socketNotifier, &QSocketNotifier::activated, this, &QClient::run, Qt::QueuedConnection);
  QObject::connect(m_eventSocketNotifier, &QSocketNotifier::activated, this, &QClient::run, Qt::QueuedConnection);
#endif

  m_eventSocketNotifier->setEnabled(false);

  emit connected();

  m_eventSocketNotifier->setEnabled(true);
}

void QClient::onDisconnected() {
  if( m_eventSocketNotifier ) {
    m_eventSocketNotifier->setEnabled(false);
    delete m_eventSocketNotifier;
  }
  if( m_socketNotifier ) {
    m_socketNotifier->setEnabled(false);
    delete m_socketNotifier;
  }
  m_socketNotifier = m_eventSocketNotifier = NULL;
  emit disconnected();
}

void QClient::run(int socket) {
  if( m_socketNotifier ) {
    m_socketNotifier->setEnabled(false);
  }
  if( m_eventSocketNotifier ) {
    m_eventSocketNotifier->setEnabled(false);
  }

  if( Client::process(1000000) < 0 ) {
    disconnect();
    return;
  }

  if( m_socketNotifier ) {
    m_socketNotifier->setEnabled(true);
  }
  if( m_eventSocketNotifier ) {
    m_eventSocketNotifier->setEnabled(true);
  }
}

void QClient::onWriteSignalValue(Signal *s, const SignalValue &v) {
  emit writeSignalValue(s, v);
}

void QClient::onWriteSignalError(Signal *s) {
  emit writeSignalError(s);
}

void QClient::onUpdateSignalValue(Signal *s, const SignalValue &v) {
  emit updateSignalValue(s, v);
}

void QClient::onUpdateSignalState(Signal *s, const SignalState &v) {
  emit updateSignalState(s, v);
}

void QClient::onAddSignal(Signal *s) {
  emit addSignal(s);
}

void QClient::onDeleteSignal(Signal *s) {
  emit deleteSignal(s);
}

