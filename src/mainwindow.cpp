#include <QSettings>
#include <QApplication>
#include <QDebug>
#include <QTimer>
#include <QMessageBox>
#include <QScrollBar>
#include <QDebug>
#include <QImage>
#include <QPixmap>
#include <QFileDialog>
#include <QTextStream>

#include <stdexcept>
#include <errno.h>

#include "mainwindow.h"
#include "signalseteditor.h"

MainWindow::MainWindow( QWidget * _parent ) :
	QMainWindow( _parent ),
	ui( new Ui::MainWindow() ),
  m_client( new QClient(this) )
{
	ui->setupUi(this);
  setWindowIcon(QIcon(":/img/circuit.png"));

  // Set up status bar
  ui->statusbar->setContentsMargins(10, 2, 10, 2);
  QLabel *dot = new QLabel("", ui->statusbar);
  dot->setObjectName("statusDot");
  QLabel *label = new QLabel("Disconnected", ui->statusbar);
  label->setObjectName("statusText");
  dot->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Preferred);
  QPixmap pixmap(QPixmap(":/img/grey_dot.png").scaledToHeight(ui->statusbar->height()/2));
  dot->setPixmap(pixmap);
  label->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Preferred);
  QWidget* spacer = new QWidget();
  spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
  ui->statusbar->addPermanentWidget(dot, 0);
  ui->statusbar->addPermanentWidget(label, 0);
  ui->statusbar->addPermanentWidget(spacer, 1);

  // Set up signal view
  SignalSetEditor *editor = new SignalSetEditor(m_client, ui->signalSets->currentWidget());
  editor->setObjectName("SignalSetEditor");
  ui->signalSets->currentWidget()->layout()->addWidget(editor);

  addAction(ui->actionNextTab);
  addAction(ui->actionPreviousTab);

  QObject::connect(ui->actionQuit, &QAction::triggered, qApp, &QApplication::quit);
  QObject::connect(ui->actionConnect, &QAction::triggered, this, &MainWindow::showConnectDialog);
  QObject::connect(ui->actionDisconnect, &QAction::triggered, this, &MainWindow::disconnect);
  QObject::connect(ui->actionAddSignalsByMask, &QAction::triggered, this, &MainWindow::addSignalByMask);
  QObject::connect(ui->actionAddSignalsByType, &QAction::triggered, this, &MainWindow::addSignalByType);
  QObject::connect(ui->actionCreateSet, &QAction::triggered, this, &MainWindow::createSet);
  QObject::connect(ui->actionPreviousTab, &QAction::triggered, [&](bool on) {
    int index = ui->signalSets->currentIndex() - 1;
    ui->signalSets->setCurrentIndex(index >= 0 ? index : ui->signalSets->count() - 1);
  });
  QObject::connect(ui->actionNextTab, &QAction::triggered, [&](bool on) {
    int index = ui->signalSets->currentIndex() + 1;
    ui->signalSets->setCurrentIndex(index >= ui->signalSets->count() ? 0 : index);
  });
  QObject::connect(ui->actionSaveAs, &QAction::triggered, this, &MainWindow::saveAs);
  QObject::connect(ui->actionSave, &QAction::triggered, this, &MainWindow::save);
  QObject::connect(ui->actionOpen, &QAction::triggered, this, &MainWindow::open);

  QObject::connect(ui->signalSets, &QTabWidget::tabBarDoubleClicked, this, &MainWindow::tabBarDoubleClicked);
  QObject::connect(ui->signalSets, &QTabWidget::tabCloseRequested, this, &MainWindow::closeTab);

  QObject::connect(m_client, &QClient::connected, this, &MainWindow::connected);
  QObject::connect(m_client, &QClient::disconnected, this, &MainWindow::disconnected);
  QObject::connect(m_client, &QClient::addSignal, this, &MainWindow::addSignal);
  QObject::connect(m_client, &QClient::deleteSignal, this, &MainWindow::deleteSignal);
  QObject::connect(m_client, &QClient::writeSignalValue, this, &MainWindow::writeSignalValue);
  QObject::connect(m_client, &QClient::writeSignalError, this, &MainWindow::writeSignalError);
  QObject::connect(m_client, &QClient::updateSignalValue, this, &MainWindow::updateSignalValue);
  QObject::connect(m_client, &QClient::updateSignalState, this, &MainWindow::updateSignalState);
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::tabBarDoubleClicked(int index) {
  m_editedTab = index;
  auto rect = ui->signalSets->tabBar()->tabRect(index);
  auto edit = new QLineEdit(ui->signalSets);
  edit->setObjectName("TabNameEditor");
  edit->show();
  edit->move(rect.left() + 2, rect.top() + 2);
  edit->resize(rect.width() - 4, rect.height() - 4);
  edit->setText(ui->signalSets->tabText(index));
  edit->selectAll();
  edit->setFocus();
  QObject::connect(edit, &QLineEdit::editingFinished, this, &MainWindow::finishRename);
}

void MainWindow::finishRename() {
  QLineEdit *edit = dynamic_cast<QLineEdit*>(sender());
  ui->signalSets->setTabText(m_editedTab, edit->text());
  edit->deleteLater();
}

void MainWindow::deleteSignal(Signal *s) {
  for(int i = 0; i < ui->signalSets->count(); i ++) {
    SignalSetEditor *editor = ui->signalSets->widget(i)->findChild<SignalSetEditor*>("SignalSetEditor");
    editor->deleteSignal(s);
  }
  delete s->userData<SignalStatus>();
}

void MainWindow::addSignal(Signal *s) {
  s->setUserData(new SignalStatus);
}

void MainWindow::disconnect(bool) {
  m_client->disconnect();
}

void MainWindow::closeTab(int index) {
  SignalSetEditor *editor = ui->signalSets->widget(index)->findChild<SignalSetEditor*>("SignalSetEditor");
  editor->deleteLater();
  ui->signalSets->removeTab(index);
}

void MainWindow::save(bool c) {
  if( m_fileName == "" ) {
    saveAs(c);
    return;
  }

  QFile file(m_fileName);

  if( !file.open(QIODevice::WriteOnly | QIODevice::Truncate) ) {
    return;
  }

  QTextStream out(&file);

  for(int i = 0; i < ui->signalSets->count(); i ++) {
    QString name = ui->signalSets->tabText(i);
    out << "[" << name << "]\n";
    SignalSetEditor *editor = ui->signalSets->widget(i)->findChild<SignalSetEditor*>("SignalSetEditor");
    editor->save(out);
  }

  file.close();
}

void MainWindow::saveAs(bool c) {

  QString fname = QFileDialog::getSaveFileName(this, "Save debug session", "session.sld", "Session file (*.sld)");

  if( fname == "" ) {
    return;
  }

  m_fileName = fname;
  save(c);
}

void MainWindow::open(bool) {
  QString fname = QFileDialog::getOpenFileName(this, "Open debug session", "", "Session file (*.sld)");

  if( fname == "" ) {
    return;
  }

  QFile file(fname);

  if( !file.open(QIODevice::ReadOnly) ) {
    return;
  }

  m_fileName = fname;

  while(ui->signalSets->count() > 0) {
    closeTab(0);
  }

  QTextStream in(&file);
  SignalSetEditor *current = NULL;

  while( !in.atEnd() ) {
    QString line = in.readLine();

    if( line[0] == '[' ) {
      QString name = line.mid(1, line.size() - 2);
      qInfo() << "Opening new tab" << name;

      QWidget *page = new QWidget(this);
      page->setLayout(new QGridLayout(page));
      page->layout()->setContentsMargins(0, 0, 0, 0);
      current = new SignalSetEditor(m_client, page);
      current->setObjectName("SignalSetEditor");
      page->layout()->addWidget(current);
      ui->signalSets->addTab(page, name);
      ui->signalSets->setCurrentIndex(ui->signalSets->count() - 1);
    }
    else if( current ) {
      Signal *s = m_client->findSignal(line.toStdString());
      if( s ) {
        current->addSignal(s);
      }
    }
  }

  file.close();
}

void MainWindow::connected() {
  QPixmap pixmap(QPixmap(":/img/green_dot.png").scaledToHeight(ui->statusbar->height()/2));
  QLabel *dot = ui->statusbar->findChild<QLabel*>("statusDot");
  dot->setPixmap(pixmap);
  m_client->postRead(SS_MASK, "");
  m_client->postProcess();
}

void MainWindow::disconnected() {
  QPixmap pixmap(QPixmap(":/img/grey_dot.png").scaledToHeight(ui->statusbar->height()/2));
  QLabel *dot = ui->statusbar->findChild<QLabel*>("statusDot");
  dot->setPixmap(pixmap);
  QLabel *label = ui->statusbar->findChild<QLabel*>("statusText");
  label->setText("Disconnected");
}

void MainWindow::connect(const QString &address, int port) {
  try {
    m_client->connect(address, port);
    QLabel *label = ui->statusbar->findChild<QLabel*>("statusText");
    label->setText(QString("Connected to ") + address + QString(":") + QString::number(port));
  }
  catch(std::exception &e) {
    qWarning() << "Connecting to" << address << ":" << port << "failed!";
  }
}

void MainWindow::createSet(bool) {
  AddSignalDialog dialog(this);
  dialog.setWindowTitle("Create signal set");

  if( !dialog.exec() ) {
    return;
  }

  QWidget *page = new QWidget(this);
  page->setLayout(new QGridLayout(page));
  page->layout()->setContentsMargins(0, 0, 0, 0);
  SignalSetEditor *editor = new SignalSetEditor(m_client, page);
  editor->setObjectName("SignalSetEditor");
  page->layout()->addWidget(editor);
  ui->signalSets->addTab(page, dialog.spec->text());
  ui->signalSets->setCurrentIndex(ui->signalSets->count() - 1);
}

void MainWindow::keyPressEvent(QKeyEvent *event) {
  if( event->key() == Qt::Key_Escape ) {
    QLineEdit *edit = findChild<QLineEdit*>("TabNameEditor");

    if( edit != NULL ) {
      if(focusWidget() == edit) {
        edit->deleteLater();
        return;
      }
    }
  }

  SignalSetEditor *editor = ui->signalSets->currentWidget()->findChild<SignalSetEditor*>("SignalSetEditor");
  if( editor ) {
    qApp->sendEvent(editor, event);
  }
}

void MainWindow::addSignalByMask(bool) {
  AddSignalDialog dialog(this);
  
  if( dialog.exec() ) {
    SignalSetEditor *editor = ui->signalSets->currentWidget()->findChild<SignalSetEditor*>("SignalSetEditor");

    if( editor ) {
      std::string mask = dialog.spec->text().toStdString();
      std::list<Signal *> signalList = (mask != "" ? m_client->findSignalsByMask(mask) : m_client->getSignals());

      for( auto s : signalList ) {
        editor->addSignal(s);
      }
    }
    else {
      qWarning() << "Signal set editor not found!";
    }
  }
}

void MainWindow::writeSignalValue(Signal *s, const SignalValue &v) {
  s->userData<SignalStatus>()->lastWrite = v;
  s->userData<SignalStatus>()->lastWriteTime = QDateTime::currentDateTime();
}

void MainWindow::writeSignalError(Signal *s) {
}

void MainWindow::updateSignalValue(Signal *s, const SignalValue &v) {
  s->userData<SignalStatus>()->lastUpdateTime = QDateTime::currentDateTime();
}

void MainWindow::updateSignalState(Signal *s, const SignalState &v) {
}

void MainWindow::addSignalByType(bool) {
  AddSignalDialog dialog(this);
  
  if( dialog.exec() ) {
    SignalSetEditor *editor = ui->signalSets->currentWidget()->findChild<SignalSetEditor*>("SignalSetEditor");

    if( editor ) {
      std::string type = dialog.spec->text().toStdString();
      std::list<Signal *> signalList = m_client->findSignalsByType(type);

      for( auto s : signalList ) {
        editor->addSignal(s);
      }
    }
    else {
      qWarning() << "Signal set editor not found!";
    }
  }
}

void MainWindow::showConnectDialog(bool) {
  ConnectDialog dialog(this);
  
  if (dialog.exec()) {
    connect(dialog.address->text(), dialog.port->value());
  }
}

