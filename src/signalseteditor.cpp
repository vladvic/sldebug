#include <QSettings>
#include <QDebug>
#include <QTimer>
#include <QMessageBox>
#include <QScrollBar>
#include <QDebug>
#include <QByteArray>
#include <QTextStream>

#include <stdexcept>
#include <errno.h>

#include "signalseteditor.h"

void SendSignalDialog::setSignalValue(const SignalValue &v) {
  m_value = v;

  switch(m_value.type()) {
  case ST_TEXT:
  case ST_BLOB:
    typeTabs->setCurrentIndex(2);
    stringValue->setText((char*)m_value);
    break;
  case ST_INT:
    typeTabs->setCurrentIndex(0);
    intValue->setValue((int64_t)m_value);
    break;
  case ST_UINT:
    typeTabs->setCurrentIndex(0);
    intValue->setValue((uint64_t)m_value);
    break;
  case ST_DOUBLE:
    typeTabs->setCurrentIndex(1);
    doubleValue->setValue((double)m_value);
    break;
  }
}

SignalValue SendSignalDialog::getSignalValue() {
  switch(m_value.type()) {
  case ST_TEXT:
  case ST_BLOB:
    m_value = stringValue->text().toStdString();
    break;
  case ST_INT:
    m_value = (int64_t)intValue->value();
    break;
  case ST_UINT:
    m_value = (uint64_t)intValue->value();
    break;
  case ST_DOUBLE:
    m_value = (double)doubleValue->value();
    break;
  }

  return m_value;
}

SignalSet::SignalSet( QClient *client )
  : m_client(client)
{
  connect(client, &QClient::writeSignalValue, this, &SignalSet::writeSignalValue);
  connect(client, &QClient::writeSignalError, this, &SignalSet::writeSignalError);
  connect(client, &QClient::updateSignalValue, this, &SignalSet::updateSignalValue);
  connect(client, &QClient::updateSignalState, this, &SignalSet::updateSignalState);
  connect(this, &SignalSet::signalUpdateNotify, this, &SignalSet::notifySignalUpdate, Qt::QueuedConnection);
}

QVariant SignalSet::valueToQVariant( const SignalValue &v ) const {
  switch( v.type() ) {
  case ST_INT:
    return QVariant((int)v);
  case ST_UINT:
    return QVariant((unsigned int)v);
  case ST_DOUBLE:
    return QVariant((double)v);
  case ST_TEXT:
    return QVariant((char*)v);
  case ST_BLOB:
    return QVariant(QByteArray((char*)v, v.size()));
  }

  return QVariant();
}

void SignalSet::addSignal( Signal *s ) {
  if( m_signals.contains( s ) ) {
    return;
  }

  if( !s->userData<SignalStatus>()->subscribed ) {
    s->subscribe(SUB_WRITE);
    s->subscribe(SUB_UPDATE);
    s->userData<SignalStatus>()->subscribed = true;
  }

  int row = m_signals.size();

  beginInsertRows(QModelIndex(), row, row);
  m_signals << s;
  endInsertRows();
}

void SignalSet::removeSignal( Signal *s ) {
  int row = m_signals.indexOf(s);

  if(row == -1) {
    return;
  }

  beginRemoveRows(QModelIndex(), row, row);
  m_signals.removeOne( s );
  endRemoveRows();
}

const QList<Signal*> &SignalSet::getSignals() const {
  return m_signals;
}

int SignalSet::rowCount(const QModelIndex &parent) const {
  return m_signals.size();
}

int SignalSet::columnCount(const QModelIndex &parent) const {
  return 5;
}

QVariant SignalSet::headerData(int section, Qt::Orientation orientation, int role) const {
  QString headerNames[] = {
    "Signal",
    "Value",
    "Last write",
    "Read error",
    "Write error"
  };

  if( role == Qt::DisplayRole && orientation == Qt::Horizontal && section < 5 ) {
    return QVariant(headerNames[section]);
  }
  else if( role == Qt::DisplayRole && orientation == Qt::Vertical ) {
    return QVariant(QString::number(section));
  }

  return QVariant();
}

Signal* SignalSet::signalByIndex(const QModelIndex &index) {
  if( m_signals.size() < index.row() ) {
    return NULL;
  }
  return m_signals.at( index.row() );
}

QVariant SignalSet::data(const QModelIndex &index, int role) const {

  if( role == Qt::DisplayRole ) {
    Signal *s = m_signals.at( index.row() );

    switch( index.column() ) {
    case 0:
      return QVariant(s->name());
    case 1:
      return valueToQVariant(s->value());
    case 2:
      return valueToQVariant(s->userData<SignalStatus>()->lastWrite);
    case 3:
      return QVariant(s->state().readFailure());
    case 4:
      return QVariant(s->state().writeFailure());
    }
  }
  else if( role == Qt::BackgroundRole ) {
    Signal *s = m_signals.at( index.row() );

    switch( index.column() ) {
    case 1:
      {
      auto diff = s->userData<SignalStatus>()->lastUpdateTime.msecsTo(QDateTime::currentDateTime());
      int d = diff * 255 / 3000;
      if( diff < 2950 ) {
        return QVariant(QBrush(QColor(d, 255, d)));
      }
      }
      break;
    case 2:
      {
      auto diff = s->userData<SignalStatus>()->lastWriteTime.msecsTo(QDateTime::currentDateTime());
      int d = diff * 255 / 3000;
      if( diff < 2950 ) {
        return QVariant(QBrush(QColor(d, 255, d)));
      }
      }
      break;
    case 3:
      if( s->state().readFailure() ) {
        return QVariant(QBrush(QColor(255, 0, 0)));
      }
      break;
    case 4:
      if( s->state().writeFailure() ) {
        return QVariant(QBrush(QColor(255, 0, 0)));
      }
      break;
    }
  }
  else if( role == Qt::ForegroundRole ) {
    Signal *s = m_signals.at( index.row() );

    switch( index.column() ) {
    case 3:
      if( s->state().readFailure() ) {
        return QVariant(QBrush(QColor(255, 255, 255)));
      }
      break;
    case 4:
      if( s->state().writeFailure() ) {
        return QVariant(QBrush(QColor(255, 255, 255)));
      }
      break;
    }
  }
  return QVariant();
}

void SignalSet::startTimer(Signal *s) {
  if( !m_signals.contains( s ) ) {
    return;
  }

  int row = m_signals.indexOf(s);
  QModelIndex topLeft = createIndex(1, row);
  QModelIndex bottomRight = createIndex(4, row);

  if(m_timers.find(row) == m_timers.end()) {
    QTimer *timer = new QTimer(this);
    timer->setSingleShot(true);
    m_timers[row] = timer;
    connect(timer, &QTimer::timeout, 
      [this, topLeft, bottomRight, s, timer] () {
        emit dataChanged(topLeft, bottomRight, QVector<int>() << Qt::BackgroundRole);
        {
        auto diff = s->userData<SignalStatus>()->lastUpdateTime.msecsTo(QDateTime::currentDateTime());
        if( diff < 3000 ) {
          timer->start(100);
          return;
        }
        }
        {
        auto diff = s->userData<SignalStatus>()->lastWriteTime.msecsTo(QDateTime::currentDateTime());
        if( diff < 3000 ) {
          timer->start(100);
          return;
        }
        }
      }
    );
  }
  else {
    m_timers[row]->stop();
  }

  m_timers[row]->start(100);
}

void SignalSet::notifySignalUpdate(Signal *s) {
  if( !m_signals.contains( s ) ) {
    return;
  }

  int row = m_signals.indexOf(s);
  QModelIndex topLeft = createIndex(1, row);
  QModelIndex bottomRight = createIndex(4, row);

  emit dataChanged(topLeft, bottomRight);

  startTimer(s);
}

void SignalSet::notifyUpdate(Signal *s) {
  emit signalUpdateNotify(s);
}

void SignalSet::writeSignalValue(Signal *s, const SignalValue &v) {
  //return;
  notifyUpdate(s);
}

void SignalSet::writeSignalError(Signal *s) {
  //return;
  notifyUpdate(s);
}

void SignalSet::updateSignalValue(Signal *s, const SignalValue &v) {
  //return;
  notifyUpdate(s);
}

void SignalSet::updateSignalState(Signal *s, const SignalState &v) {
  //return;
  notifyUpdate(s);
}

SignalSetEditor::SignalSetEditor( QClient *client, QWidget * parent )
  : m_client(client)
  , m_signalSet(client)
  , m_recursionLock(false)
{
  setupUi(this);
  tableView->setModel(&m_signalSet); 
  tableView->setColumnWidth(0, 400);
  connect(tableView, &QTableView::doubleClicked, this, &SignalSetEditor::writeUpdateSignal);
}

SignalSetEditor::~SignalSetEditor() {
  qInfo() << "Closing signal editor";
}

void SignalSetEditor::writeUpdateSignal(const QModelIndex &index) {
  Signal *s = m_signalSet.signalByIndex(index);

  SendSignalDialog dialog(this);
  dialog.setSignalValue(s->value());

  if( dialog.exec() ) {
    bool update = dialog.Ui::SendSignal::update->isChecked();
    auto value = dialog.getSignalValue();

    if( update ) {
      s->update(value);
    }
    else {
      s->write(value);
    }
  }

  QTimer *timer = new QTimer(this);
  timer->singleShot(200, [timer, s, this]() {
    m_signalSet.notifyUpdate(s);
    timer->deleteLater();
  });
}

void SignalSetEditor::save(QTextStream &f) {
  for( auto signal : m_signalSet.getSignals() ) {
    f << QString(signal->name()) << QString("\n");
  }
}

void SignalSetEditor::keyPressEvent(QKeyEvent *event) {
  if( event->key() == Qt::Key_Delete ){
    QItemSelectionModel *model = tableView->selectionModel();
    QModelIndexList list = model->selectedIndexes();
    QModelIndex current = list.first();

    QList<Signal*> deleted;

    for( auto &idx : list ) {
      Signal *s = m_signalSet.getSignals().at(idx.row());
      deleted << s;
    }

    for( auto s : deleted ) {
      m_signalSet.removeSignal(s);
    }

    if( m_signalSet.rowCount(QModelIndex()) >= current.row() ) {
      tableView->setCurrentIndex(current);
    }
    return;
  }
  else if( event->matches(QKeySequence::Copy) ) {
    QItemSelectionModel *model = tableView->selectionModel();
    QModelIndexList list = model->selectedIndexes();

    if( list.size() < 2 ) {
      Signal *s = m_signalSet.signalByIndex(tableView->currentIndex());
      if( s ) {
        QString text = s->name();
        QApplication::clipboard()->setText(text);
      }
    }
    else {
      QString text = "";
      for( auto &idx : list ) {
        Signal *s = m_signalSet.signalByIndex(idx);
        if( s ) {
          text += s->name();
          text += "\n";
        }
      }
      QApplication::clipboard()->setText(text);
    }
    return;
  }
  else if( event->matches(QKeySequence::Paste) ) {
    QString text = QApplication::clipboard()->text();
    QStringList rowContents = text.split("\n", QString::SkipEmptyParts);

    for( QString &name : rowContents ) {
      Signal *s = m_client->findSignal(name.toStdString());

      if( s ) {
        m_signalSet.addSignal(s);
      }
    }
    return;
  }

  if( m_recursionLock ) {
    return;
  }
  m_recursionLock = true;
  qApp->sendEvent(tableView, event);
  m_recursionLock = false;
}

void SignalSetEditor::addSignal(Signal *s) {
  m_signalSet.addSignal(s);
}

void SignalSetEditor::deleteSignal(Signal *s) {
  m_signalSet.removeSignal(s);
}

