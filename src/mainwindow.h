/*
 * mainwindow.h - header file for MainWindow class
 *
 * Copyright (c) 2009-2014 Tobias Doerffel / Electronic Design Chemnitz
 *
 * This file is part of QModBus - http://qmodbus.sourceforge.net
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (see COPYING); if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA.
 *
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QDateTime>
#include <QFile>

#include "QClient.h"
#include "ui_mainwindow.h"
#include "ui_connectdialog.h"
#include "ui_addsignaldialog.h"

class MainWindow;

extern MainWindow * globalMainWin;

class ConnectDialog : public QDialog, public Ui::ConnectDialog
{
public:
  ConnectDialog(QWidget * _parent) :
      QDialog(_parent)
  {
    setupUi(this);
  }
};

class AddSignalDialog : public QDialog, public Ui::AddSignalDialog
{
public:
  AddSignalDialog(QWidget * _parent) :
      QDialog(_parent)
  {
    setupUi(this);
  }
};

struct SignalStatus {
  SignalStatus() 
    : subscribed(false)
    , lastUpdateTime(QDateTime::fromMSecsSinceEpoch(0))
    , lastWriteTime(QDateTime::fromMSecsSinceEpoch(0)) { }
  bool subscribed;
  SignalValue lastWrite;
  QDateTime lastUpdateTime;
  QDateTime lastWriteTime;
};

class MainWindow : public QMainWindow
{
  Q_OBJECT
public:
  MainWindow( QWidget * parent = 0 );
  ~MainWindow();

private slots:
  void showConnectDialog(bool);
  void addSignalByMask(bool);
  void addSignalByType(bool);
  void disconnect(bool);
  void createSet(bool);
  void finishRename();
  void closeTab(int);

  void save(bool);
  void saveAs(bool);
  void open(bool);

  void connected();
  void disconnected();
  void addSignal(Signal *s);
  void deleteSignal(Signal *s);

  void writeSignalValue(Signal *s, const SignalValue &v);
  void writeSignalError(Signal *s);
  void updateSignalValue(Signal *s, const SignalValue &v);
  void updateSignalState(Signal *s, const SignalState &v);

private:
  void connect(const QString &address, int port);
  void keyPressEvent(QKeyEvent *event);
  void tabBarDoubleClicked(int index);

  QString m_fileName;
  Ui::MainWindow* ui;
  QLabel *m_statusText;
  QClient *m_client;
  int m_editedTab;
};

#endif // MAINWINDOW_H
