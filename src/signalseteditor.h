/*
 * mainwindow.h - header file for SignalSetEditor class
 *
 * Copyright (c) 2009-2014 Tobias Doerffel / Electronic Design Chemnitz
 *
 * This file is part of QModBus - http://qmodbus.sourceforge.net
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (see COPYING); if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA.
 *
 */

#ifndef SIGNALSETEDITOR_H
#define SIGNALSETEDITOR_H

#include <QSet>
#include <QLabel>
#include <QAbstractTableModel>
#include <QTimer>
#include <QKeyEvent>
#include <QFile>
#include <QClipboard>
#include <QItemSelectionModel> 

#include "QClient.h"
#include "mainwindow.h"
#include "ui_signalseteditor.h"
#include "ui_sendsignal.h"

class SendSignalDialog : public QDialog, public Ui::SendSignal
{
public:
  SendSignalDialog(QWidget * _parent) :
      QDialog(_parent)
  {
    setupUi(this);
    typeTabs->tabBar()->hide();
  }

  void setSignalValue(const SignalValue &v);
  SignalValue getSignalValue();

private:
  SignalValue m_value;
};

class SignalSet : public QAbstractTableModel
{
  Q_OBJECT

  QVariant valueToQVariant( const SignalValue &v ) const;
  void startTimer(Signal *s);

public:
  SignalSet( QClient *m_client );

  void addSignal( Signal *s );
  void removeSignal( Signal *s );
  const QList<Signal*> &getSignals() const;
  Signal* signalByIndex(const QModelIndex &index);

  int rowCount(const QModelIndex &parent) const;
  int columnCount(const QModelIndex &parent) const;
  QVariant data(const QModelIndex &index, int role) const;
  QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

  void notifyUpdate(Signal *s);

signals:
  void signalUpdateNotify(Signal *s);

private slots:
  void notifySignalUpdate(Signal *s);
  void writeSignalValue(Signal *s, const SignalValue &v);
  void writeSignalError(Signal *s);
  void updateSignalValue(Signal *s, const SignalValue &v);
  void updateSignalState(Signal *s, const SignalState &v);

private:
  QList<Signal*> m_signals;
  QMap<int, QTimer*> m_timers;
  QClient *m_client;
};

class SignalSetEditor : public QWidget, public Ui::SignalSetEditor
{
  Q_OBJECT
public:
  SignalSetEditor( QClient *m_client, QWidget * parent = 0 );
  ~SignalSetEditor();

  void addSignal(Signal *s);
  void deleteSignal(Signal *s);

  void save(QTextStream &f);

private slots:
  void writeUpdateSignal(const QModelIndex &index);

private:
  void keyPressEvent(QKeyEvent *event);

  QClient *m_client;
  SignalSet m_signalSet;
  bool m_recursionLock;
};

#endif // SIGNALSETEDITOR_H
