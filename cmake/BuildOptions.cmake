set(CMAKE_CXX_STANDARD 11)

find_package(Threads REQUIRED)

set(SYSTEM_LIBRARIES  "")

if(APPLE OR WIN32)
  set(SYSTEM_LIBRARIES "")
else(APPLE OR WIN32)
  find_package(LibRt)
  set(SYSTEM_LIBRARIES ${LIBRT_LIBRARIES})
endif(APPLE OR WIN32)

if(NOT WIN32)
  add_definitions(-fPIC)
else(NOT WIN32)
  set(SYSTEM_LIBRARIES  ws2_32 wsock32)
endif(NOT WIN32)

option(ENABLE_PROFILING "Enable profiling"   OFF)
option(ENABLE_DEBUG     "Include debug info" ON)
option(ENABLE_SHARED    "Build dynamic libraries" OFF)
option(ENABLE_OPTIMIZATION "Optimized build" ON)
option(BUILD_BINARIES "Build binaries (as opposed to library only)" ON)

if(ENABLE_SHARED)
  set(BUILDTYPE SHARED)
else(ENABLE_SHARED)
  set(BUILDTYPE STATIC)
endif(ENABLE_SHARED)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x")

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fdiagnostics-show-option -Wall -Wno-unknown-pragmas -Wno-pragmas")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fdiagnostics-show-option -Wall -Wno-unknown-pragmas -Wno-pragmas")

if(ENABLE_PROFILING)
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g -pg")
  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -g -pg")
  set(PROGRAM_SUFFIX ".profile")
else(ENABLE_PROFILING)
  set(PROGRAM_SUFFIX "")
  if(ENABLE_DEBUG)
    set(PROGRAM_SUFFIX ".debug")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g")
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -g")
  endif(ENABLE_DEBUG)
endif(ENABLE_PROFILING)

if(ENABLE_OPTIMIZATION)
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O3")
  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -O3")
endif(ENABLE_OPTIMIZATION)

if(NOT CMAKE_RUNTIME_OUTPUT_DIRECTORY)
  if(ENABLE_SHARED)
    set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/bin${PROGRAM_SUFFIX})
  endif(ENABLE_SHARED)

  set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/bin${PROGRAM_SUFFIX})
endif(NOT CMAKE_RUNTIME_OUTPUT_DIRECTORY)

