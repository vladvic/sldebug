
macro(run_configure TARGET_NAME FOLDER GENFILE)
  set(OPTIONS ${ARGN})
  execute_process(COMMAND ${CMAKE_C_COMPILER} -dumpmachine OUTPUT_VARIABLE MACHINE OUTPUT_STRIP_TRAILING_WHITESPACE)
  add_custom_command(OUTPUT ${FOLDER}/${GENFILE}.host.${MACHINE}
                     COMMAND rm -f ${FOLDER}/${GENFILE}.host.* &&
                             touch ${FOLDER}/${GENFILE}.host.${MACHINE} &&
                             rm -f ${FOLDER}/${GENFILE})
  add_custom_command(OUTPUT ${FOLDER}/${GENFILE}
                     COMMAND CC=${CMAKE_C_COMPILER} CXX=${CMAKE_CXX_COMPILER} ${FOLDER}/configure ${OPTIONS}
                            --host=`${CMAKE_C_COMPILER} -dumpmachine`
                     WORKING_DIRECTORY ${FOLDER}
                     DEPENDS ${TARGET_NAME}_${MACHINE})
  add_custom_target(${TARGET_NAME}_${MACHINE} ALL DEPENDS ${FOLDER}/${GENFILE}.host.${MACHINE})
  add_custom_target(${TARGET_NAME} ALL DEPENDS ${FOLDER}/${GENFILE})
endmacro(run_configure)

